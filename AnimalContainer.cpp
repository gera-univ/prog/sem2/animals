//
// Created by herman on 5/7/20.
//

#include <map>
#include <iostream>

#include "AnimalContainer.h"

void AnimalContainer::addAnimal(std::string owner, std::string specie, std::string name, int age) {
    auto entry = std::vector<std::string>(4);
    mData.emplace_back(Animal(owner, specie, name, age));
}

std::vector<std::pair<std::string, size_t>> AnimalContainer::queryOwnerSpecieCounts() {
    std::map<std::string, std::set<std::string>> speciesByOwner;
    for (auto &animal : mData) {
        speciesByOwner[animal.owner].insert(animal.specie);
    }
    std::vector<std::pair<std::string, size_t>> specieCountsByOwner;
    specieCountsByOwner.reserve(speciesByOwner.size());
    for (auto const&[owner, species] : speciesByOwner) {
        specieCountsByOwner.emplace_back(owner, species.size());
    }
    std::sort(specieCountsByOwner.begin(), specieCountsByOwner.end(), [](auto &left, auto &right) {
        return left.second > right.second;
    });
    return specieCountsByOwner;
}

std::pair<std::set<std::string>, std::set<std::string>> AnimalContainer::querySpecieOwnersAndNames(std::string specie) {
    std::set<std::string> owners, names;
    for (auto &animal : mData) {
        if (animal.specie == specie) {
            owners.insert(animal.owner);
            names.insert(animal.name);
        }
    }
    return {owners, names};
}

size_t AnimalContainer::queryNameSpecieCount(std::string name) {
    std::set<std::string> species;
    for (auto &animal : mData) {
        if (animal.name == name)
            species.insert(animal.specie);
    }
    return species.size();
}

std::vector<std::pair<std::string, std::pair<int, int>>> AnimalContainer::queryYoungestAndOldestBySpecie() {
    std::map<std::string, std::vector<int>> agesBySpecie;
    for (auto &animal : mData) {
        agesBySpecie[animal.specie].emplace_back(animal.age);
    }
    std::vector<std::pair<std::string, std::pair<int, int>>> youngestAndOldestBySpecie;
    for (auto[specie, ages]:agesBySpecie) {
        auto[min, max]=std::minmax_element(ages.begin(), ages.end());
        youngestAndOldestBySpecie.push_back({specie,{*min,*max}});
    }
    return youngestAndOldestBySpecie;
}
