//
// Created by herman on 5/7/20.
//

#ifndef ANIMALS_ANIMALCONTAINER_H
#define ANIMALS_ANIMALCONTAINER_H

#include <vector>
#include <string>
#include <set>
#include "Animal.h"

class AnimalContainer {
public:
    void addAnimal(std::string owner, std::string specie, std::string name, int age);

    std::vector<std::pair<std::string, size_t>> queryOwnerSpecieCounts();

    size_t queryNameSpecieCount(std::string name);

    std::vector<std::pair<std::string, std::pair<int, int>>> queryYoungestAndOldestBySpecie();

    std::pair<std::set<std::string>, std::set<std::string>> querySpecieOwnersAndNames(std::string specie);

private:
    std::vector<Animal> mData;
};


#endif //ANIMALS_ANIMALCONTAINER_H
