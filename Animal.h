//
// Created by herman on 5/7/20.
//

#ifndef ANIMALS_ANIMAL_H
#define ANIMALS_ANIMAL_H

#include <string>

struct Animal {
    std::string owner;
    std::string specie;
    std::string name;
    int age;

    Animal(std::string owner, std::string specie, std::string name, int age) : owner(owner), specie(specie), name(name),
                                                                               age(age) {}
};

#endif //ANIMALS_ANIMAL_H
