#include <iostream>
#include <fstream>
#include <sstream>
#include "AnimalContainer.h"

void PerformQuery1(AnimalContainer &animals);

void PerformQuery2(AnimalContainer &animals, std::string &specie);

void PerformQuery3(AnimalContainer &animals, std::string &name);

void PerformQuery4(AnimalContainer &animals);

using namespace std;

int main(int argc, char **argv) {
    ifstream fin("input.txt");

    if (!fin.is_open()) {
        cerr << "input.txt was not found in the same directory as the executable: " << argv[0];
    }

    AnimalContainer animals;
    std::string owner, specie, name, ageString, _comma;

    while (getline(fin, owner, ',')) {
        getline(fin, specie, ',');
        getline(fin, name, ',');
        getline(fin, ageString);
        std::istringstream ss(ageString);
        int age;
        ss >> age;

        animals.addAnimal(owner, specie, name, age);
    }

    cout
            << "1\t\t\t\tGet the number of different species by owner\n" <<
            "2\tSpecie\t\tGet all the owners of the Specie\n" <<
            "3\tName\t\tCount the different species of the Name\n" <<
            "4\t\t\t\tGet the oldest and the youngest animal of every specie\n"
            "q\t\t\t\tQuit\n\n";
    std::string input;
    while (input != "q") {
        cin >> input;
        if (input == "1")
            PerformQuery1(animals);
        else if (input == "2") {
            cout << "Enter species name\n";
            cin >> input;
            PerformQuery2(animals, input);
        } else if (input == "3") {
            cout << "Enter pet name\n";
            cin >> input;
            PerformQuery3(animals, input);
        } else if (input == "4")
            PerformQuery4(animals);
        else if (input == "q")
            break;
        else {
            cerr << "Incorrect input\n";
        }
        cout << endl;
    }
}

void PerformQuery4(AnimalContainer &animals) {
    auto q4 = animals.queryYoungestAndOldestBySpecie();
    for (auto[specie, ages]:q4) {
        auto[oldest, youngest]=ages;
        if (oldest == youngest)
            cout << "All species of " << specie << " are " << youngest << "\n";
        else
            cout << "Oldest " << specie << " is " << oldest << "\n" << "Youngest " << specie << " is " << youngest
                 << "\n";
    }
}

void PerformQuery3(AnimalContainer &animals, string &name) {
    auto q3 = animals.queryNameSpecieCount(name);
    cout << "There are " << q3 << " species with the name " << name;
}

void PerformQuery2(AnimalContainer &animals, std::string &specie) {
    auto q2 = animals.querySpecieOwnersAndNames(specie);
    const auto&[owners, names]=q2;
    cout << "Owners:";
    for (auto owner:owners) {
        cout << owner << " ";
    }
    cout << "\nNames:";
    for (auto name:names) {
        cout << name << " ";
    }
    cout << "\n";
}

void PerformQuery1(AnimalContainer &animals) {
    auto q1 = animals.queryOwnerSpecieCounts();
    for (auto[owner, count] : q1) {
        cout << owner << ":" << count << "\n";
    }
}
