cmake_minimum_required(VERSION 3.15)
project(Animals)

set(CMAKE_CXX_STANDARD 20)

add_executable(test_animals main.cpp AnimalContainer.cpp AnimalContainer.h Animal.h)

configure_file(input.txt input.txt COPYONLY)
